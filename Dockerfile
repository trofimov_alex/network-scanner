FROM python:3.12.3
COPY ./requirements.txt /app/
COPY ./network-scanner.py /app/
	# Устанавливаем доп. библиотеки Python и утилиту ping в контейнер
WORKDIR /app
RUN pip install -r requirements.txt && \
    apt-get update && apt-get install iputils-ping -y
ENTRYPOINT ["python","network-scanner.py"]