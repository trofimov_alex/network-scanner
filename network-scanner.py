import os
import argparse
import requests
import json
# Импортируем необходимые библиотеки 

# в зависимости от типа ОС, на которой запускается код, будут разные параметры запуска утилиты ping | nt = Windows
command = "ping -n 2" if os.name == "nt" else "ping -c 2"
# Сканируем сеть
def do_ping_sweep(ip, num_of_host):
    # делим IP на октеты - адрес сети и адрес хоста
    ip_parts = ip.split('.')
    network_ip = ip_parts[0] + '.' + ip_parts[1] + '.' + ip_parts[2] + '.'
    # при вызове функции в качестве 2 параметра будет итерируемый номер хоста
    host_ip = network_ip + str(int(ip_parts[3]) + num_of_host)
    # используем командную строку
    response = os.popen(f'{command} {host_ip}')
    # результат (ответ) записываем в res
    res = response.readlines()
    print(f"[#] Result of scanning: {host_ip} [#]\n{res[2]}", end='\n')

# Производим отправку HTTP запросов
def sent_http_request(target, method, headers=None, payload=None):
     # создаем пустой словарь
     headers_dict = dict()
     # headers - список из строки с заголовками, разделенной пробелами (пробелы в значении заголовка недопустимы?)
     if headers:
         for header in headers:
             header_name = header.split(':')[0]     # выделяем название заголовка (строка)
             header_value = header.split(':')[1:]   # записываем значение заголовка (список)
             headers_dict[header_name] = ':'.join(header_value)     # если в значении был символ двоеточия
     # стандартный методы библиотеки requests. Передаем аргументы из CMD
     if method == "GET":
         response = requests.get(target, headers=headers_dict)
     elif method == "POST":
         response = requests.post(target, headers=headers_dict, data=payload)
     else:
         print(f"Не выбран метод GET или POST для ресурса {target}")
         return
     print(
         f"[#] Response status code: {response.status_code}\n"
         f"[#] Response headers: {json.dumps(dict(response.headers), indent=4, sort_keys=True)}\n"
         # CHANGED BY ME (выводим в консоль только первые 100 символов, остальное - в файл)
         f"[#] Response content (only first 100 symbols):\n {response.text[:100]}"
    )
     # CHANGED BY ME
     # запись полного Response в файл на диске
     with open("Response_content.txt", "w") as file:
         file.write(response.text)

# парсинг параметров из команды, введенной в терминале
parser = argparse.ArgumentParser(description='Network scanner')
parser.add_argument('task', choices=['scan', 'sendhttp'], help='Network scan or send HTTP request')
parser.add_argument('-i', '--ip', type=str, default='192.168.31.1', help='IP address')
parser.add_argument('-n', '--num_of_hosts', type=int, default = 1, help='Number of hosts')
parser.add_argument('-t', '--target', type=str, help='URL')
parser.add_argument('-m', '--method', type=str, help='Method')
# nargs='*' - неопределенное кол-во параметров
parser.add_argument('-hd', '--headers', type=str, nargs='*', help='Headers')
args = parser.parse_args()

# CHANGED BY ME (добавлена проверка корректности ввода)
# проверка корректности IP адреса
def IP_validation(ip_address):
    try:
        # создаем список, элементы = каждый октет x.x.x.x IP-адреса
        ip_list = list(map(int, ip_address.split(".")))
        if len(ip_list) != 4:
            raise ValueError(f"Check IP address {ip_address} (should be x.x.x.x).")
        for octet in ip_list:
            if octet not in range(1,255):
                raise ValueError(f"Check IP address {ip_address} (decimal number can be in range 1-254).")
    except ValueError as exc:
        print(f"Wrong arguments. {exc}")
        return 0
    return 1

# Описание главной функции с 2 опциями (scan или sendhttp)
def main():
    if args.task == 'scan':
        # CHANGED BY ME (try - except)
        try:
            if IP_validation(args.ip):
                print("Please wait. Sending requests...")
            for host_num in range(args.num_of_hosts):
                do_ping_sweep(args.ip, host_num)
        except Exception as err:
            raise Exception(err)

    elif args.task == 'sendhttp':
        # CHANGED BY ME (try - except)
        try:
            sent_http_request(args.target, args.method, args.headers)
        except Exception as err:
            raise Exception(err)

if __name__ == "__main__":
    try:
        main()
    except Exception as err:
        print(f"Failed to execute script [error: {err}]")


#   do_ping_sweep
# python 2024-05-11_network-scanner.py scan -i 192.168.31.1 -n 1
#   sent_http_request
# python 2024-05-11_network-scanner.py sendhttp -t https://google.com -m GET -hd Accept-Language:ru,en User-Agent:Mozilla/5.0 Accept:text/html

# HOW TO export requirements
# pip freeze > requirements.txt